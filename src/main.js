import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import {
  Select,
  Option,
} from 'view-design';
import 'view-design/dist/styles/iview.css';

Vue.component('Select', Select);
Vue.component('Option', Option);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')