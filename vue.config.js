const webpack = require('webpack');
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '' : '/',
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = '一键换肤'
        return args
      })
    config.plugin('provide').use(webpack.ProvidePlugin, [{
      axios: 'axios',
    }]);
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          require('postcss-px2rem')({ //配置项，详见官方文档
            remUnit: 100
          }), // 换算的基数
        ]
      },
      sass: {
        // 根据自己样式文件的位置调整
        prependData: `@import "@/assets/css/scss.scss";`
      }
    }
  },
}